package bp;

import java.security.InvalidParameterException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import db.Database;
import db.Parameter;

public class Toy implements IToy, IPermanentStorage {

	private int toyID;
	private String inspector;
	private Date inspectionDateTime;
	private Circuit circuit1 = new Circuit(1);
	private Circuit circuit2 = new Circuit(2);

	@Override
	public int getToyID() {
		return toyID;
	}

	@Override
	public String getInspector() {
		return inspector;
	}

	@Override
	public Date getInspectionDateTime() {
		return inspectionDateTime;
	}

	@Override
	public Circuit getCircuit1() {
		return circuit1;
	}

	@Override
	public Circuit getCircuit2() {
		return circuit2;
	}

	@Override
	public void setToyID(int pToyID) {
		toyID = pToyID;
		circuit1.setToyID(pToyID);
		circuit2.setToyID(pToyID);
	}

	@Override
	public void setInspector(String pInspector) {
		inspector = pInspector;
	}

	@Override
	public void setInspectionDateTime(Date pInspectionDateTime) {
		inspectionDateTime = pInspectionDateTime;
	}

	@Override
	public void setCircuit1(Circuit pCircuit1) {
		circuit1 = pCircuit1;
	}

	@Override
	public void setCircuit2(Circuit pCircuit2) {
		circuit2 = pCircuit2;
	}

	@Override
	public void save() {
		Database db = new Database();
		List<Parameter> params = new ArrayList<>();

		// set the parameter values from the properties
		params.add(new Parameter<Integer>(toyID));
		params.add(new Parameter<String>(inspector));
		params.add(new Parameter<Date>(inspectionDateTime));

		// save
		db.executeSql("usp_SaveToy", params);
		circuit1.save();
		circuit2.save();

	}

	@Override
	public void clear() {

	}

	@Override
	public void delete() {
		Database db = new Database();
		List<Parameter> params = new ArrayList<>();
		params.add(new Parameter<Integer>(toyID));
		db.executeSql("usp_DeleteToy", params);
		
		
		
	}

	@Override
	public void load(int... id) {
		Database db = new Database();
		List<Parameter> params = new ArrayList<>();

		params.add(new Parameter<Integer>(id[0]));

		ResultSet toys = db.getResultSet("usp_GetToy", params);

		try {
			if (toys.next()) {
				toyID = toys.getInt("ToyID");
				inspectionDateTime = toys.getDate("InspectionDateTime");
				inspector = toys.getString("Inspector");
				circuit1.load(toyID, 1);
				circuit2.load(toyID, 2);

			} else {
				throw new InvalidParameterException("ToyID not found");
			}
		} catch (SQLException e) {
			e.printStackTrace();

		}
	}
}

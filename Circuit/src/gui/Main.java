/*

Purpose: We will calculate either the amperage,
 voltage, or resistance of a circuit given the other two parameters.

Step 1: Ask the user which value they want to calculate
Step 2: Depending on the input above, ask for the other
 relevant values (e.g. if the user wants to calculate voltage,
  ask for known values of amperage and resistance, etc.)
Step 3: Output the correctly calculated
 value of the variable specified in Step 1.

*/

//What will this program do?
//What does the user want to calculate?
//Get their input
//if they choose 1 calculate Resistance
//get the Amperage and Voltage to calculate it.
//if they choose 2 calculate Voltage
//get the Amperage and Resistance
//if they choose 3 calculate Amperage
//get the Resistance and Voltage
//ask if they want to run it again otherwise terminate
//get the input response

package gui;

import java.util.Scanner;

import bp.Circuit;

/**
 * main class that interacts with the user.
 *
 * @author 189913
 *
 */
public final class Main {

	/**
	 * This is here literally to just remove the error.
	 */
	private Main() {

	}

	/**
	 * replacement for the number 3.
	 */
	public static final int NUMBER_THREE = 3;
	/**
	 * replacement for the number 4.
	 */
	public static final int NUMBER_FOUR = 4;

	/**
	 * Only class that interacts with the user.
	 *
	 * @param args
	 *            args
	 */
	public static void main(final String[] args) {
		Scanner key = new Scanner(System.in);
		Circuit myCircuit = new Circuit();
		int input = 0;

		// What will this program do?
		System.out
				.println("This program will calculate the"
						+ " Resistance,"
						+ " Voltage, or Amperage"
						+ " of any given circuit.");
		do {
			// What does the user want to calculate?
			System.out.println("");
			System.out.println("What would you like to calculate?");
			System.out.println("1) Resistance");
			System.out.println("2) Voltage");
			System.out.println("3) Amperage");
			System.out.println("Enter the number you would"
				+ " like to calculate and press enter.");

			// Get their input
			input = key.nextInt();
			if (input > NUMBER_THREE || input < 1) {
				System.out.println("");
				System.out.println("That is an incorrect "
						+ "response.");
			}

		} while (input > NUMBER_THREE || input < 1);

		while (input <= NUMBER_THREE || input >= 1) {

			// if they choose 1 calculate Resistance
			// get the Amperage and Voltage to calculate it.
			if (input == 1) {
				System.out.println("");
				System.out.println("You have chosen to"
				+ " calculate Resistance");
				System.out.println("What is the Amperage?");
				myCircuit.setAmperage(key.nextDouble());
				System.out.println("What is the Voltage?");
				myCircuit.setVoltage(key.nextDouble());
				myCircuit.calculateResistance();
				System.out.println("The Resistance is "
				+ myCircuit.getResistance() + " Ohms.");
			}

			// if they choose 2 calculate Voltage
			// get the Amperage and Resistance
			if (input == 2) {
				System.out.println("");
				System.out.println("You have chosen to "
				+ "calculate Voltage");
				System.out.println("What is the Amperage?");
				myCircuit.setAmperage(key.nextDouble());
				System.out.println("What is the Resistance?");
				myCircuit.setResistance(key.nextDouble());
				myCircuit.calculateVoltage();
				System.out.println("The Voltage is "
				+ myCircuit.getVoltage() + " Volts");
			}

			// if they choose 3 calculate Amperage
			// get the Resistance and Voltage
			if (input == NUMBER_THREE) {
				System.out.println("");
				System.out.println("You have chosen to "
				+ "calculate Amperage");
				System.out.println("What is the Resistance?");
				myCircuit.setResistance(key.nextDouble());
				System.out.println("What is the Voltage?");
				myCircuit.setVoltage(key.nextDouble());
				myCircuit.calculateAmperage();
				System.out.println("The Amperage is "
				+ myCircuit.getAmperage() + " Amps");
			}

			do {
				// ask if they want to run it again
				//otherwise terminate
				System.out.println("");
				System.out.println("Would you like to run"
						+ " another calculation?");
				System.out.println("1) Yes Calculate"
						+ " Resistance");
				System.out.println("2) Yes Calculate Voltage");
				System.out.println("3) Yes Calculate Amperage");
				System.out.println("4) No, End Program");

				// get the input response
				input = key.nextInt();

				if (input == NUMBER_FOUR) {
					key.close();
					return;
				}

			} while (input < 1 || input > NUMBER_THREE);
		}

		key.close();
	}
}

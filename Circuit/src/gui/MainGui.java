package gui;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import bp.Circuit;

import javax.swing.JButton;
import java.awt.Window.Type;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JSeparator;

public class MainGui {

	private JFrame frmOhmsLawCalculator;
	private JTextField txtBxAmperage;
	private JTextField txtBxVoltage;
	private JTextField txtBxResistance;
	private JLabel lblVoltage;
	private JLabel lblResistance;
	private Circuit myCircuit = new Circuit();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGui window = new MainGui();
					window.frmOhmsLawCalculator.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmOhmsLawCalculator = new JFrame();
		frmOhmsLawCalculator.getContentPane().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent key) {
				if (KeyEvent.VK_ESCAPE == key.getKeyChar()) {
					// clear the actual visual text boxes
					txtBxAmperage.setText("");
					txtBxVoltage.setText("");
					txtBxResistance.setText("");

					// clear the values
					myCircuit.clearValues();
				}
			}
		});
		frmOhmsLawCalculator.setType(Type.UTILITY);
		frmOhmsLawCalculator.setTitle("Ohms Law Calculator");
		frmOhmsLawCalculator.setResizable(false);
		frmOhmsLawCalculator.setBounds(100, 100, 322, 249);
		frmOhmsLawCalculator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		txtBxAmperage = new JTextField();
		txtBxAmperage.setColumns(10);
		txtBxAmperage.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent key) {
				if (KeyEvent.VK_ESCAPE == key.getKeyChar()) {
					// clear the actual visual text boxes
					txtBxAmperage.setText("");
					txtBxVoltage.setText("");
					txtBxResistance.setText("");
					txtBxAmperage.setBackground(Color.WHITE);
					txtBxResistance.setBackground(Color.WHITE);
					txtBxVoltage.setBackground(Color.WHITE);

					// clear the values
					myCircuit.clearValues();
				}
				if (key.getKeyChar() < KeyEvent.VK_0 || key.getKeyChar() > KeyEvent.VK_9) {
					if (key.getKeyChar() != KeyEvent.VK_PERIOD) {
						key.consume();
					}
				} if (key.getKeyChar() == KeyEvent.VK_PERIOD && txtBxAmperage.getText().contains(".")) {
					key.consume();
				}
					

			}
		});

		txtBxVoltage = new JTextField();
		txtBxVoltage.setColumns(10);
		txtBxVoltage.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent key) {
				if (KeyEvent.VK_ESCAPE == key.getKeyChar()) {
					// clear the actual visual text boxes
					txtBxAmperage.setText("");
					txtBxVoltage.setText("");
					txtBxResistance.setText("");
					txtBxAmperage.setBackground(Color.WHITE);
					txtBxResistance.setBackground(Color.WHITE);
					txtBxVoltage.setBackground(Color.WHITE);

					// clear the values
					myCircuit.clearValues();
				}
				if (key.getKeyChar() < KeyEvent.VK_0 ||key.getKeyChar() >KeyEvent.VK_9) {
					if (key.getKeyChar() != KeyEvent.VK_PERIOD) {
						key.consume();
					}
				} if (key.getKeyChar() == KeyEvent.VK_PERIOD && txtBxVoltage.getText().contains(".")) {
					key.consume();
				}
			}
		});

		txtBxResistance = new JTextField();
		txtBxResistance.setColumns(10);
		txtBxResistance.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent key) {
				if (KeyEvent.VK_ESCAPE == key.getKeyChar()) {
					// clear the actual visual text boxes
					txtBxAmperage.setText("");
					txtBxVoltage.setText("");
					txtBxResistance.setText("");
					
					txtBxAmperage.setBackground(Color.WHITE);
					txtBxResistance.setBackground(Color.WHITE);
					txtBxVoltage.setBackground(Color.WHITE);

					// clear the values
					myCircuit.clearValues();
				}
				if (key.getKeyChar() < KeyEvent.VK_0 ||key.getKeyChar() > KeyEvent.VK_9) {
					if (key.getKeyChar() != KeyEvent.VK_PERIOD) {
						key.consume();
					}
				} if (key.getKeyChar() == KeyEvent.VK_PERIOD && txtBxResistance.getText().contains(".")) {
					key.consume();
				}
			}
		});

		JLabel lblAmperage = new JLabel("Amperage");

		lblVoltage = new JLabel("Voltage");

		lblResistance = new JLabel("Resistance");

		JButton btnCalculate = new JButton("Calculate");
		btnCalculate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DecimalFormat fs = new DecimalFormat("0.####");
				
				if (txtBxAmperage.getText().equals(".") || txtBxVoltage.getText().equals(".")
						|| txtBxResistance.getText().equals(".")) {
					JOptionPane.showMessageDialog(null, "Please do not leave one decimal in the space.");
					return;
				}


				if (!txtBxAmperage.getText().equals("") && !txtBxVoltage.getText().equals("")
						&& !txtBxResistance.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Please leave one space empty.");
				}

				if (txtBxAmperage.getText().equals("") && txtBxVoltage.getText().equals("")
						&& txtBxResistance.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Please Enter 2 values");
					return;
				}

				if (txtBxAmperage.getText().equals("") && txtBxVoltage.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Please Enter 2 values");
					return;
				}

				if (txtBxVoltage.getText().equals("") && txtBxResistance.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Please Enter 2 values");
					return;
				}

				if (txtBxAmperage.getText().equals("")) {
					txtBxAmperage.setBackground(Color.WHITE);
					myCircuit.setVoltage(Double.parseDouble(txtBxVoltage.getText()));
					myCircuit.setResistance(Double.parseDouble(txtBxResistance.getText()));
					myCircuit.calculateAmperage();
					txtBxAmperage.setText(fs.format(myCircuit.getAmperage() + "Amps"));
					txtBxAmperage.setBackground(Color.GREEN);
				}

				if (txtBxVoltage.getText().equals("")) {
					txtBxAmperage.setBackground(Color.WHITE);
					myCircuit.setResistance(Double.parseDouble(txtBxResistance.getText()));
					myCircuit.setAmperage(Double.parseDouble(txtBxAmperage.getText()));
					myCircuit.calculateVoltage();
					txtBxVoltage.setText(fs.format(myCircuit.getVoltage() + "Volts"));
					txtBxVoltage.setBackground(Color.GREEN);
				}

				if (txtBxResistance.getText().equals("")) {
					txtBxResistance.setBackground(Color.WHITE);
					myCircuit.setAmperage(Double.parseDouble(txtBxAmperage.getText()));
					myCircuit.setVoltage(Double.parseDouble(txtBxVoltage.getText()));
					myCircuit.calculateResistance();
					txtBxResistance.setText(fs.format(myCircuit.getResistance() + "Ohms"));
					txtBxResistance.setBackground(Color.GREEN);
				}
			}

		});

		JLabel lblEnterTheTwo = new JLabel(
				"<html>\nEnter the two known values and leave the third empty. Then click calculate.\n</html>");

		JButton btnClearValues = new JButton("Clear Values");
		btnClearValues.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// clear the actual visual text boxes
				txtBxAmperage.setText("");
				txtBxVoltage.setText("");
				txtBxResistance.setText("");
				
				txtBxAmperage.setBackground(Color.WHITE);
				txtBxResistance.setBackground(Color.WHITE);
				txtBxVoltage.setBackground(Color.WHITE);

				// clear the values
				myCircuit.clearValues();
			}
		});
		
		JSeparator separator = new JSeparator();
		
		JSeparator separator_1 = new JSeparator();
		
		JSeparator separator_2 = new JSeparator();

		GroupLayout groupLayout = new GroupLayout(frmOhmsLawCalculator.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(24)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lblAmperage)
										.addComponent(lblVoltage)
										.addComponent(lblResistance))
									.addGap(76)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(txtBxResistance, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(txtBxVoltage, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(txtBxAmperage, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
											.addGap(106)
											.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
												.addComponent(separator_1, GroupLayout.PREFERRED_SIZE, 1, GroupLayout.PREFERRED_SIZE)
												.addComponent(separator_2, GroupLayout.PREFERRED_SIZE, 1, GroupLayout.PREFERRED_SIZE)))
										.addComponent(btnCalculate))
									.addPreferredGap(ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
									.addComponent(btnClearValues))))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblEnterTheTwo, GroupLayout.PREFERRED_SIZE, 290, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(separator, GroupLayout.PREFERRED_SIZE, 1, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(separator_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblEnterTheTwo, GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAmperage)
						.addComponent(txtBxAmperage, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblVoltage, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtBxVoltage, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblResistance)
						.addComponent(txtBxResistance, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(btnClearValues)
						.addComponent(separator_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnCalculate))
					.addGap(28))
		);
		frmOhmsLawCalculator.getContentPane().setLayout(groupLayout);

		frmOhmsLawCalculator.getRootPane().setDefaultButton(btnCalculate);
	}
}

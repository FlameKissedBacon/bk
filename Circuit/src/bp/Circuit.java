package bp;


/**
 * The logic class containing all the.
 * assessors and mutators
 * @author cberkstresser
 *
 */
public class Circuit implements ICircuit {

    /**
     * stores the voltage.
     */
    private double pVoltage = 0.0;
	/**
	 * stores the amperage.
	 */
	private double pAmperage = 0.0;
	/**
	 * stores the resistance.
	 */
	private double pResistance = 0.0;

	// return the voltage
	@Override
	public final double getVoltage() {
		return pVoltage;
	}

	// return the amperage
	@Override
	public final double getAmperage() {
		return pAmperage;
	}

	// return the resistance
	@Override
	public final double getResistance() {
		return pResistance;
	}

	// save the voltage
	@Override
	public final void setVoltage(final double voltage) {
		pVoltage = voltage;
	}

	// save the amperage
	@Override
	public final void setAmperage(final double amperage) {
		pAmperage = amperage;
	}

	// save the reistance
	@Override
	public final void setResistance(final double resistance) {
		pResistance = resistance;

	}

	// use this to calculate the voltage
	@Override
	public final void calculateVoltage() {
		pVoltage = pAmperage * pResistance;
	}

	// use this to calculate the amperage
	@Override
	public final void calculateAmperage() {
		pAmperage = (pVoltage / pResistance);
	}

	// use this to calculate the resistance
	@Override
	public final void calculateResistance() {
		pResistance = (pVoltage / pAmperage);
	}

	// use this to clear all the variables
	/**
	 * used to clear the values of the three variables.
	 */
	public final void clearValues() {
		pResistance = 0.0;
		pAmperage = 0.0;
		pVoltage = 0.0;
	}
}

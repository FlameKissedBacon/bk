package Gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import bp.Toy;

import javax.swing.JPanel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.border.TitledBorder;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JSeparator;
import javax.swing.JProgressBar;
import javax.swing.JComboBox;

public class Main {

	private JFrame frmToyCircuitData;
	private JTextField txtbxInspector;
	private JTextField txtbxDate;
	private JTextField txtC1Res;
	private JTextField txtC2Volt;
	private JTextField txtC2Res;
	private JTextField txtC1Volt;
	private JComboBox<String> cboC1ManufactureLocation;
	private JComboBox<String> cboC2ManufactureLocation;
	private JLabel lblStatus;
	
	private static String[] COUNTRIES = {"China", "Germany","United States"};

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frmToyCircuitData.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmToyCircuitData = new JFrame();
		frmToyCircuitData.setTitle("Toy Circuit Data Collector");
		frmToyCircuitData.setBounds(100, 100, 450, 324);
		frmToyCircuitData.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmToyCircuitData.getContentPane().setLayout(null);
		
		JLabel lblInspector = new JLabel("Inspector:");
		lblInspector.setBounds(12, 12, 99, 15);
		frmToyCircuitData.getContentPane().add(lblInspector);
		
		JLabel lblDate = new JLabel("Date:");
		lblDate.setBounds(12, 39, 70, 15);
		frmToyCircuitData.getContentPane().add(lblDate);
		
		txtbxInspector = new JTextField();
		txtbxInspector.setBounds(107, 10, 114, 19);
		frmToyCircuitData.getContentPane().add(txtbxInspector);
		txtbxInspector.setColumns(10);
		
		txtbxDate = new JTextField();
		txtbxDate.setEnabled(false);
		txtbxDate.setText("<calculated>");
		txtbxDate.setEditable(false);
		txtbxDate.setBounds(107, 39, 114, 19);
		frmToyCircuitData.getContentPane().add(txtbxDate);
		txtbxDate.setColumns(10);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new BtnSaveActionListener());
		btnSave.setBounds(158, 244, 117, 25);
		frmToyCircuitData.getContentPane().add(btnSave);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Circuit 1", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 91, 186, 123);
		frmToyCircuitData.getContentPane().add(panel);
		panel.setLayout(null);
		
		cboC1ManufactureLocation = new JComboBox<String>();
		cboC1ManufactureLocation.setModel(new DefaultComboBoxModel<>(COUNTRIES));
		cboC1ManufactureLocation.setBounds(12, 87, 149, 24);
		panel.add(cboC1ManufactureLocation);
		
		txtC1Res = new JTextField();
		txtC1Res.setBounds(93, 51, 70, 19);
		panel.add(txtC1Res);
		txtC1Res.setColumns(10);
		
		JLabel lblC1Resistance = new JLabel("Resistance");
		lblC1Resistance.setBounds(12, 53, 85, 14);
		panel.add(lblC1Resistance);
		
		JLabel lblC1Voltage = new JLabel("Voltage");
		lblC1Voltage.setBounds(12, 24, 78, 14);
		panel.add(lblC1Voltage);
		
		txtC1Volt = new JTextField();
		txtC1Volt.setBounds(93, 22, 70, 19);
		panel.add(txtC1Volt);
		txtC1Volt.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Circuit 2", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.setBounds(230, 91, 194, 123);
		frmToyCircuitData.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		cboC2ManufactureLocation = new JComboBox<String>();
		cboC2ManufactureLocation.setModel(new DefaultComboBoxModel<>(COUNTRIES));
		cboC2ManufactureLocation.setBounds(12, 87, 149, 24);
		panel_1.add(cboC2ManufactureLocation);
		
		JLabel lblC2Voltage = new JLabel("Voltage");
		lblC2Voltage.setBounds(12, 27, 78, 14);
		panel_1.add(lblC2Voltage);
		
		txtC2Volt = new JTextField();
		txtC2Volt.setBounds(101, 22, 70, 19);
		panel_1.add(txtC2Volt);
		txtC2Volt.setColumns(10);
		
		JLabel lblC2Resistance = new JLabel("Resistance");
		lblC2Resistance.setBounds(12, 50, 78, 14);
		panel_1.add(lblC2Resistance);
		
		txtC2Res = new JTextField();
		txtC2Res.setBounds(101, 48, 70, 19);
		panel_1.add(txtC2Res);
		txtC2Res.setColumns(10);
		frmToyCircuitData.getContentPane().setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{txtbxInspector, txtC1Volt, txtC1Res, txtC2Volt, txtC2Res, btnSave}));
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 80, 414, 11);
		frmToyCircuitData.getContentPane().add(separator);
		
		lblStatus = new JLabel("");
		lblStatus.setBounds(263, 12, 46, 14);
		frmToyCircuitData.getContentPane().add(lblStatus);
	}
	private class BtnSaveActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			Toy myToy = new Toy();
			
			lblStatus.setText("Saved and Calcuted");
			myToy.setInspector(txtbxInspector.getText());
			
			myToy.setInspectionDateTime((new Date()));
			
			//set circuit 1 values
			myToy.getCircuit1().setVoltage(Double.parseDouble(txtC1Volt.getText()));
			myToy.getCircuit1().setResistance(Double.parseDouble(txtC1Res.getText()));
			myToy.getCircuit1().calculateAmperage();
			
			myToy.getCircuit1().setManufactureLocation(cboC1ManufactureLocation.getSelectedItem().toString());
			myToy.getCircuit2().setManufactureLocation(cboC1ManufactureLocation.getSelectedItem().toString());
			myToy.getCircuit1().setManufactureLocation(cboC2ManufactureLocation.getSelectedItem().toString());
			myToy.getCircuit2().setManufactureLocation(cboC2ManufactureLocation.getSelectedItem().toString());
			
			//set circuit 2 values
			
		}
		//TODO add keylistener to all fields
		private class TxtDoubleValueKeyListener extends KeyAdapter {
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_ESCAPE) {
					clearForm();
				} //end if
			}//end keyAdapter
		}// end keylistener
		
		private void clearForm() {
			//TODO Clear all fields
		}
	}
}

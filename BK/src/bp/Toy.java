package bp;

import java.util.Date;

public class Toy implements IToy {
	
	private int toyID;
	private String inspector;
	private Date inspectionDateTime;
	private Circuit circuit1 = new Circuit(1);
	private Circuit circuit2 = new Circuit(2);

	@Override
	public int getToyID() {
		return toyID;
	}

	@Override
	public String getInspector() {
		return inspector;
	}

	@Override
	public Date getInspectionDateTime() {
		return inspectionDateTime;
	}

	@Override
	public Circuit getCircuit1() {
		return circuit1;
	}

	@Override
	public Circuit getCircuit2() {
		return circuit2;
	}

	@Override
	public void setToyID(int pToyID) {
		toyID = pToyID;
	}

	@Override
	public void setInspector(String pInspector) {
		inspector = pInspector;
	}

	@Override
	public void setInspectionDateTime(Date pInspectionDateTime) {
		inspectionDateTime = pInspectionDateTime;
	}

	@Override
	public void setCircuit1(Circuit pCircuit1) {
		circuit1 = pCircuit1;
	}

	@Override
	public void setCircuit2(Circuit pCircuit2) {
		circuit2 = pCircuit2;
	}

}

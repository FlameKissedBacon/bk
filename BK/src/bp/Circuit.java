package bp;

public class Circuit implements ICircuit {
	private int toyID;
	private int circuitID;
	private double amperage;
	private double voltage;
	private double resistance;
	private String manufactureLocation;
	
	public Circuit(int pCircuitID) {
		circuitID = pCircuitID;
	}
	
	

	@Override
	public int getToyID() {
		return toyID;
	}

	@Override
	public int getCircuitID() {
		return circuitID;
	}

	@Override
	public double getVoltage() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getAmperage() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getResistance() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getManufactureLocation() {
		return manufactureLocation;
	}

	@Override
	public void setToyID(int ptoyID) {
		toyID = ptoyID;
	}

	@Override
	public void setCircuitID(int pCircuitID) {
		circuitID = pCircuitID;
	}

	@Override
	public void setVoltage(double pVoltage) {
		voltage = pVoltage;
	}

	@Override
	public void setAmperage(double pAmperage) {
		amperage = pAmperage;
	}

	@Override
	public void setResistance(double pResistance) {
		resistance = pResistance;
	}

	@Override
	public void setManufactureLocation(String pManufactureLocation) {
			manufactureLocation = pManufactureLocation;
	}

	@Override
	public void calculateVoltage() {
		voltage = amperage * resistance;
	}

	@Override
	public void calculateAmperage() {
		amperage = voltage / resistance;
	}

	@Override
	public void calculateResistance() {
		resistance = voltage / amperage;
	}

	@Override
	public boolean isValid() {
		if (voltage == amperage * resistance) {
			return true;
		} else
			return false;
	}

}
